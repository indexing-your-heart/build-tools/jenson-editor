# JensonEditor

![Screenshot of the plugin in Godot](./screenshot.png)
The JensonEditor plugin allows you to edit dialogue scripts in the Jenson
file format directly in the Godot editor, without needing to use a
separate tool such as Gutenberg.

> **Important**  
> Currently, this plugin is compatible with Apple Silicon Macs running
> macOS Ventura (13.0) or later.

## Getting started

Download the latest release of the plugin [in the Releases page][release]
and unzip the contents of the archive.

In your Godot project, copy the `jenson_editor` directory to your project's
addons directory:

```
res://
╰─ addons
   ╰─ jenson_editor
      ╰─ ... 
```

In the Godot editor, you will be prompted to reload the current project to
enable extensions. Tap 'Restart' to restart the editor, then Go to the
**Project > Project Settings** menu and open the 'Plugins' tab. Finally,
enable the JensonEditor plugin in the list of available plugins.

[release]: https://gitlab.com/indexing-your-heart/build-tools/jenson-editor/-/releases

## License

Like Indexing Your Heart, JensonEditor is licensed under the Cooperative
Non-violent Public License. You can read your rights in the LICENSE.md
file in this repository.