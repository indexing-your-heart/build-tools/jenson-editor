build-extension:
	#!/bin/zsh
	cd EditorSource
	swift build --configuration release
	cp .build/release/libJensonEditor.dylib ../Testbed/addons/jenson_editor
	cp -af .build/release/SwiftGodotCore.framework ../Testbed/addons/jenson_editor/SwiftGodotCore.framework
	cd ..