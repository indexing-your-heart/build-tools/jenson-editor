// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "EditorSource",
    platforms: [.macOS(.v13)],
    products: [
        .library(
            name: "JensonEditor",
            type: .dynamic,
            targets: ["JensonEditor"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/Indexing-Your-Heart/core-dependencies/JensonKit",
                 branch: "root"),
        .package(url: "https://gitlab.com/Indexing-Your-Heart/engine/SwiftGodot",
                 branch: "root"),
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .target(
            name: "JensonEditor",
            dependencies: [
                "JensonKit",
                .product(name: "SwiftGodot", package: "SwiftGodot")]),
    ]
)
