//
//  JensonEditorExtension.swift
//  JensonEditor
//
//  Created by Marquis Kurt on 29/10/23.
//
//  This file is part of JensonEditor.
//
//  JensonEditor is non-violent software: you can use, redistribute, and/or modify it under the terms of the
//  CNPLv7+ as found in the LICENSE file in the source code root directory or at
//  <https://git.pixie.town/thufie/npl-builder>.
//
//  JensonEditor comes with ABSOLUTELY NO WARRANTY, to the extent permitted by applicable law. See the CNPL for
//  details.

import SwiftGodot

@GodotMain
class JensonEditorExtension: GodotExtensionDelegate {
    func extensionDidInitialize(at level: SwiftGodotCore.GDExtension.InitializationLevel) {
        if level == .editor {
            register(type: JensonEditor.self)
        }
    }
}
