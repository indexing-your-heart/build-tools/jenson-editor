//
//  JensonEditor.swift
//  JensonEditor
//
//  Created by Marquis Kurt on 29/10/23.
//
//  This file is part of JensonEditor.
//
//  JensonEditor is non-violent software: you can use, redistribute, and/or modify it under the terms of the
//  CNPLv7+ as found in the LICENSE file in the source code root directory or at
//  <https://git.pixie.town/thufie/npl-builder>.
//
//  JensonEditor comes with ABSOLUTELY NO WARRANTY, to the extent permitted by applicable law. See the CNPL for
//  details.

import JensonKit
import SwiftGodot

@NativeHandleDiscarding
class JensonEditor: MarginContainer {
    enum FileMenuItem: Int64 {
        case open
        case close
        case save
        case saveAs
    }

    enum ViewMenuItem: Int64 {
        case showSpaces
        case showTabs
    }

    @SceneTree(path: "VBoxContainer/EditorSplit/TextEditor") private var editor: CodeEdit?

    @SceneTree(path: "VBoxContainer/Menu/StoryTitle") private var storyTitle: Label?
    @SceneTree(path: "VBoxContainer/Menu/DocPortal") private var docPortal: Button?
    @SceneTree(path: "VBoxContainer/Menu/FloatWindow") private var floatWindow: Button?
    @SceneTree(path: "VBoxContainer/Menu/MenuBar/File") private var fileMenu: PopupMenu?
    @SceneTree(path: "VBoxContainer/Menu/MenuBar/View") private var viewMenu: PopupMenu?
    @SceneTree(path: "VBoxContainer/Menu/MenuBar/Open") private var openPicker: FileDialog?
    @SceneTree(path: "VBoxContainer/Menu/MenuBar/Save") private var savePicker: FileDialog?

    @SceneTree(path: "VBoxContainer/StatusBar/CurrentFile") private var currentFileLabel: Label?
    @SceneTree(path: "VBoxContainer/StatusBar/SaveState") private var saveStateLabel: Label?

    var currentEditorPath: String = ""
    var currentFile: Substring {
        currentEditorPath.split(separator: "/").last ?? ""
    }
    var currentMetadata: JensonStory?

    required init() {
        super.init()
    }

    override func _ready() {
        addThemeConstantOverride(name: "margin_top", constant: 4)
        addThemeConstantOverride(name: "margin_left", constant: 8)
        addThemeConstantOverride(name: "margin_bottom", constant: 4)
        addThemeConstantOverride(name: "margin_right", constant: 8)
        do {
            try fileMenu?.idPressed.connect { [weak self] idx in
                self?.fileMenuPressed(at: idx)
            }
            try viewMenu?.idPressed.connect { [weak self] idx in
                self?.viewMenuPressed(at: idx)
            }
            try openPicker?.fileSelected.connect { [weak self] path in
                self?.loadFile(from: path)
            }
            try savePicker?.fileSelected.connect { [weak self] path in
                self?.saveFile(to: path)
            }
            try docPortal?.pressed.connect {
                do {
                    try OS.shellOpen(uri: "https://jenson.indexingyourhe.art/documentation/jensonkit")
                } catch {
                    GD.pushError("Could not open docs: \(error.localizedDescription)")
                }
            }
            try floatWindow?.pressed.connect { [weak self] in
                self?.presentAsModalWindow()
            }
        } catch {
            GD.pushError("Couldn't set up editor: \(error.localizedDescription)")
        }
    }

    func closeCurrentFile() {
        guard let editor, let currentFileLabel else { return }
        editor.text = ""
        currentEditorPath = ""
        currentFileLabel.text = "No file open."
        for menuItem in [FileMenuItem.close, FileMenuItem.save, FileMenuItem.saveAs] {
            fileMenu?.setItemDisabled(index: Int32(menuItem.rawValue), disabled: true)
        }
        storyTitle?.text = ""
    }

    func enableEditor(with text: String, at path: String) {
        guard let editor, let currentFileLabel else { return }
        editor.text = text
        currentEditorPath = path
        currentFileLabel.text = String(currentFile)
        for menuItem in [FileMenuItem.close, FileMenuItem.save, FileMenuItem.saveAs] {
            fileMenu?.setItemDisabled(index: Int32(menuItem.rawValue), disabled: false)
        }
        storyTitle?.text = "\(currentMetadata?.name ?? "Untitled Story")"
        if let currentMetadata, let chapter = currentMetadata.chapter {
            storyTitle?.text += " - \(chapter)"
        }
    }

    func loadFile(from path: String) {
        do {
            let reader = try JensonReader(resource: path)
            if let data = try reader?.decode() {
                let encoder = JSONEncoder()
                encoder.outputFormatting = [.prettyPrinted, .sortedKeys]
                let realString = try String(data: encoder.encode(data), encoding: .utf8)
                currentMetadata = data.story
                self.enableEditor(with: realString ?? "{}", at: path)
            }
        } catch {
            GD.pushError("Could not load file contents: \(error.localizedDescription)")
        }
    }

    func saveCurrentFile() {
        saveFile(to: currentEditorPath)
    }

    func saveFile(to path: String) {
        guard let text = editor?.text else {
            GD.pushError("Couldn't get editor text value: \(String(describing: editor?.text))")
            return
        }
        guard let data = text.data(using: .utf8) else {
            GD.pushError("Unable to convert save to data.")
            return
        }

        let decoder = JSONDecoder()
        do {
            let file = try decoder.decode(JensonFile.self, from: data)
            let writer = JensonWriter(contentsOf: file)
            try writer.write(resource: path)
            saveStateLabel?.text = "File saved."
        } catch {
            GD.pushError("Could not save file: \(error.localizedDescription)")
            saveStateLabel?.text = "Error: \(error.localizedDescription)"
        }
    }

    func fileMenuPressed(at index: Int64) {
        guard let item = FileMenuItem(rawValue: index) else { return }
        switch item {
        case .open:
            openPicker?.show()
        case .close:
            closeCurrentFile()
        case .save:
            saveCurrentFile()
        case .saveAs:
            savePicker?.show()
        }
    }

    func viewMenuPressed(at index: Int64) {
        guard let item = ViewMenuItem(rawValue: index) else { return }
        switch item {
        case .showSpaces:
            editor?.drawSpaces.toggle()
            viewMenu?.toggleItemChecked(index: Int32(ViewMenuItem.showSpaces.rawValue))
        case .showTabs:
            editor?.drawTabs.toggle()
            viewMenu?.toggleItemChecked(index: Int32(ViewMenuItem.showTabs.rawValue))
        }
    }

    func presentAsModalWindow() {
        let originalParent = getParent()
        let newWindow = Window()
        newWindow.title = "Jenson Dialogue Editor - Godot"

        getParent()?.addChild(node: newWindow)
        getParent()?.removeChild(node: self)
        floatWindow?.hide()
        newWindow.addChild(node: self)

        let windowSize = Vector2i(x: Int32(getRect().size.x), y: Int32(getRect().size.x))
        newWindow.popupCentered(minsize: windowSize)

        do {
            try newWindow.closeRequested.connect { [weak self] in
                newWindow.removeChild(node: self)
                originalParent?.addChild(node: self)
                self?.floatWindow?.show()
                newWindow.hide()
            }
        } catch {
            GD.pushError("Unable to connect close signal: \(error.localizedDescription)")
        }
    }
}
